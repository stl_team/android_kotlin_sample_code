package com.kotlindemo.rss

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.kotlindemo.R
import com.kotlindemo.login.APIServices
import com.kotlindemo.login.LoginResponse
import kotlinx.android.synthetic.main.activity_rss.*
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

class RssActivity : AppCompatActivity(),SwipeRefreshLayout.OnRefreshListener {


    private val rssList:List<RssFeedModel>?=null
    private var apiServices:APIServices?=null
    private var compositeSubscription:CompositeSubscription?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rss)
        init()
    }

    fun init()
    {
        compositeSubscription= CompositeSubscription()
        apiServices= APIServices.createRss()
        //recyclerView.setLayoutManager( LinearLayoutManager(this));
        recyclerView.layoutManager=LinearLayoutManager(this)
        btnFeed.setOnClickListener(View.OnClickListener { v -> callFeeAPI() })
        swipeRefreshLayout.setOnRefreshListener(this)
    }

    fun callFeeAPI()
    {
       if(!rssFeedEditText.text.isEmpty()) {
           val subscription = apiServices?.rssFeed("xkdc.comm/rss.xml")
                   ?.subscribeOn(Schedulers.io())
                   ?.observeOn(AndroidSchedulers.mainThread())
                   //  ?.onErrorResumeNext(Func1<Throwable, Observable<out Any>> { throwable -> Observable.error<LoginResponse>(throwable) })
                   ?.subscribe(object : Subscriber<RssFeedModel>() {
                       override fun onCompleted() {

                       }

                       override fun onError(e: Throwable) {
                           e.printStackTrace()
                       }

                       override fun onNext(rssFeedModel: RssFeedModel?) {
                           tvFeedTitle.setText("Feed Title: " + rssFeedModel?.title);
                           tvFeedDescription.setText("Feed Description: " + rssFeedModel?.description);
                           feedLink.setText("Feed Link: " + rssFeedModel?.link);
                           recyclerView.adapter=RssFeedAdapter()
                       }
                   })
           compositeSubscription!!.add(subscription)
       }
    }

    override fun onPause() {
        super.onPause()
        compositeSubscription?.unsubscribe()
    }
    override fun onRefresh() {
        callFeeAPI()
    }

}

