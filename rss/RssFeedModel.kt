package com.kotlindemo.rss

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

/**
 * Created by ravi on 16/2/18.
 */
@Root(name="channel")
class RssFeedModel {

    @Element(name = "title")
    var title:String?=null

    @Element(name="description")
    var description:String?=null

    @Element(name = "link")
    var link:String?=null

    @Element(name = "lastBuildDate")
    var lastBuildDate:String?= null

    @Element(name="pubDate")
    var pubDate:String?=null

    @Element(name = "ttl")
    var ttl:Int?=null

    @ElementList(name = "item")
    val item:List<RssFeedModel>?=null

    @Root(name = "item")
    inner class Item
    {
        @Element(name = "title")
        var title:String?=null

        @Element(name = "description")
        var description:String?=null

        @Element(name = "link")
        var link:String?=null

        @Element(name = "guid")
        var guid:Boolean?=null

        @Element(name = "pubDate")
        var pubDate:String?=null

    }

}