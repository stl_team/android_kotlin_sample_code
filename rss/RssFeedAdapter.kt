package com.kotlindemo.rss

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kotlindemo.R

/**
 * Created by ravi on 16/2/18.
 */
class RssFeedAdapter: RecyclerView.Adapter<RssFeedAdapter.FeedModelViewHolder>
{
    private var rssList:List<RssFeedModel>?=null

    constructor(rssListModel:List<RssFeedModel>)
    {
        this.rssList=rssListModel
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): FeedModelViewHolder{
        val v = LayoutInflater.from(parent?.getContext())
                .inflate(R.layout.row_item_rss, parent, false)
       return FeedModelViewHolder(v)
    }

    override fun getItemCount(): Int {
       return rssList!!.size
    }

    override fun onBindViewHolder(holder: FeedModelViewHolder?, position: Int) {

        val  rssFeed:RssFeedModel=rssList!!.get(position)
        val tvTitle:TextView=holder?.itemView!!.findViewById(R.id.titleText)
        val tvDesc:TextView=holder?.itemView!!.findViewById(R.id.descriptionText)
        val tvLink:TextView=holder?.itemView!!.findViewById(R.id.linkText)
        tvTitle.text=rssFeed.title
        tvDesc.text=rssFeed.description
        tvLink.text=rssFeed.link

    }

    inner class FeedModelViewHolder: RecyclerView.ViewHolder
    {
        private var rssView:View?=null
        constructor(view: View) : super(view) {
            this.rssView=view
        }

    }

}


