package com.kotlindemo.login
import android.support.annotation.XmlRes
import com.kotlindemo.rss.RssFeedModel
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import retrofit2.http.*
import rx.Observable

/**
 * Created by ravi on 14/2/18.
 */
interface APIServices {

    /**
     * Login API
     *
     */
    @FormUrlEncoded
    @POST("check_member_login")
    fun LoginCheck(@FieldMap map: Map<String, String>): Observable<LoginResponse>

    @FormUrlEncoded
    @POST("{url_endpoint}")
    fun rssFeed(@Path("url_endpoint") path: String):Observable<RssFeedModel>



    companion object {
        fun create(): APIServices {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://dev.shivlab.com/nrisamaj/api/")
                    .build()

            return retrofit.create(APIServices::class.java)
        }

        fun createRss():APIServices
        {
            val retrofit =  Retrofit.Builder()
                    .baseUrl("http://")
                    .client(OkHttpClient())
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build();
            return retrofit.create(APIServices::class.java)
        }
    }



}