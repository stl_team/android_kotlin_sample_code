package com.kotlindemo.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by ravi on 14/2/18.
 */
class LoginResponse {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null
    @SerializedName("info")
    @Expose
    var info: Info? = null


    inner class Info {
        @SerializedName("id_users")
        @Expose
        private var idUsers: String? = null
        @SerializedName("fullname")
        @Expose
        private var fullname: String? = null
        @SerializedName("email_id")
        @Expose
        private var emailId: String? = null
        @SerializedName("password")
        @Expose
        private var password: String? = null
        @SerializedName("phone_number")
        @Expose
        private var phoneNumber: String? = null
        @SerializedName("country_code")
        @Expose
        private var countryCode: String? = null
        @SerializedName("country")
        @Expose
        private var country: String? = null
        @SerializedName("state")
        @Expose
        private var state: String? = null
        @SerializedName("city")
        @Expose
        private var city: String? = null
        @SerializedName("profile_picture")
        @Expose
        private var profilePicture: String? = null
        @SerializedName("status")
        @Expose
        private var status: String? = null
        @SerializedName("created_date")
        @Expose
        private var createdDate: String? = null
        @SerializedName("updated_date")
        @Expose
        private var updatedDate: String? = null

    }

}