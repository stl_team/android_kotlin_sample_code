package com.kotlindemo

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.kotlindemo.login.APIServices
import com.kotlindemo.login.LoginResponse
import kotlinx.android.synthetic.main.activity_main.*
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription


class LoginActivity : AppCompatActivity() {

    private var context: Context? = null
    private var apiServices: APIServices? = null
    private var compositeSubscription: CompositeSubscription? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        context = this
        compositeSubscription = CompositeSubscription()
        apiServices = APIServices.create();
        btnLogin.setOnClickListener(View.OnClickListener { v -> callingLoginAPI() })
    }


    override fun onPause() {
        super.onPause()
        compositeSubscription?.unsubscribe()
    }


    fun callingLoginAPI() {
        if (edtEmail!!.text.isEmpty()) {
            Toast.makeText(context, "Please enter E-mail", Toast.LENGTH_LONG).show()
        } else if (edtPassword!!.text.isEmpty()) {
            Toast.makeText(context, "Please enter Password", Toast.LENGTH_LONG).show()
        } else {
            val fieldMap = HashMap<String, String>()
            fieldMap["email_id"] = edtEmail.text.toString()
            fieldMap["password"] = edtPassword.text.toString()
            fieldMap["device_id"] = "123"
            fieldMap["os_flag"] = "2"


            var subscription = apiServices?.LoginCheck(fieldMap)
                    ?.subscribeOn(Schedulers.io())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    //  ?.onErrorResumeNext(Func1<Throwable, Observable<out Any>> { throwable -> Observable.error<LoginResponse>(throwable) })
                    ?.subscribe(object : Subscriber<LoginResponse>() {
                        override fun onCompleted() {

                        }

                        override fun onError(e: Throwable) {
                            e.printStackTrace()
                        }

                        override fun onNext(loginResponseData: LoginResponse) {
                            if (loginResponseData != null) {
                                Toast.makeText(context, loginResponseData.message, Toast.LENGTH_LONG).show()
                            }
                        }
                    })
            compositeSubscription!!.add(subscription)
        }

    }


}






